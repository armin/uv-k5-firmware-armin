#!/bin/sh
user="$(whoami)"
docker build -t uvk5 .
docker run --rm -v ${PWD}/compiled-firmware:/app/compiled-firmware uvk5 /bin/bash -c "cd /app && make && cp firmware* compiled-firmware/"
a="$(date +"%y%m%d_%H%M%S")"
f="firmware_custom__${a}.packed.bin"
sudo chown -R "$user" compiled-firmware/
mv -v compiled-firmware/firmware.packed.bin "$f" && \
echo "New firmware file is at: $f"


